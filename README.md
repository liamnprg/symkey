# symkey

A terrible symmetric key crypto system


# Usage

```
symkey 1.0
Liam Naddell <liam.naddell@mail.utoronto.ca>
a hash based symmetric cryptography system

USAGE:
    symkey [FLAGS] <KEY>

FLAGS:
    -d, --decrypt    decrypts data from stdin using KEY
    -e, --encrypt    encrypts data from stdin using KEY
    -h, --help       Prints help information
    -k, --keygen     Generates new key and writes key to KEY
    -V, --version    Prints version information

ARGS:
    <KEY>    Sets the input file to use
```

## Example

* Key generation: `symkey -k keyfile`
* Encryption: `symkey -e keyfile < plaintext > encryptedmessage`
* Decryption: `symkey -d keyfile < encryptedmessage`

*Can you tell I just finished working on a cpio implementation*



# How it works!

## Encryption

1. The plaintext is divided into two-character (ascii-only) blocks.
2. The key is then cut with the plaintext.
3. The key and the plaintext are hashed using sha384. 
4. The hashes are written to stdout

## Decryption

1. Load hash list and key into memory
2. Foreach block, guess every possible plaintext until the key cut with the plaintext hashes to the correct hash
3. Print plaintext to stdout. 

Decryption is in linear time with an average of 32,512.5 sha384 hashes per character (`255^2` hashes per block) 

# Notice

This software is provided as-is with no guarantee of reliability, security, or hardware-safety. This software involves heavy cpu usage and may damage hardware if used for extended periods of time, especially with encrypting large messages.

# (Potential) Vulnerabilities

* Because the key is hashed so many times, it might be possible to reverse the key from a large set of hashes, maybe if one knows the plaintext.
* Because the key-cutting algorithm is very basic, it might be more easy to un-hash the key.
* Since plaintext is cut into two-character ascii strings, hashes will repeat themselves, which might provide insight into the plaintext given a large enough sample size (something similar to encrypting Advanced Programming in the UNIX Enviornment or the bible)
* Mitigation: Cut the plaintext into 128 characters and add them together later

# todo

* I don't know how to do this, but if one was interested in gpgpu acceleration, this project would benefit greatly from it. 
