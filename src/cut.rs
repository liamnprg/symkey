pub const PTSIZE: usize = 2;
pub const KEYSIZE: usize = 256-PTSIZE;

pub fn cut(key: &[u8],pt: &[u8;PTSIZE]) -> Vec<u8> {
    let mut cut: [u8;256] = [0;256];
    let mut i = 0;
    let mut keyindice = 0;
    while i<256 {
        if i%(256/PTSIZE) == 0 && i < 252 {
            cut[i] = pt[i/(256/PTSIZE)];
        } else {
            cut[i] = key[keyindice];
            keyindice+=1;
        }
        i+=1;
    }
    let fin = Vec::from(cut);
    return fin;
}
