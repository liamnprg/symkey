use rand::Rng;
use std::io::Write;
use crate::cut::KEYSIZE;

pub fn keygen(keyfile: &str) {
    let mut key: Vec<u8> = Vec::with_capacity(KEYSIZE);
    key.resize(KEYSIZE,0);

    let mut rng = rand::thread_rng();

    for x in key.iter_mut() {
        *x = rng.gen();
    }
    let mut file = std::fs::File::create(keyfile).expect("create failed");
    file.write_all(&key[..]).expect("write failed");
}
