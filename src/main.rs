use clap::{Arg, App};
use std::io::Read;
use crate::encrypt::encrypt_block;
use std::fs;
use crate::keygen::keygen;
use crate::decrypt::decrypt;
use crate::cut::{PTSIZE,KEYSIZE};

use serde::{Serialize, Deserialize};

mod keygen; 
mod encrypt;
mod decrypt;
mod cut;


#[derive(Serialize, Deserialize, Debug)]
struct EncryptedMessage {
    hashes: Vec<String>,
}

fn main() {
    let matches = App::new("symkey")
                          .version("1.0")
                          .author("Liam Naddell <liam.naddell@mail.utoronto.ca>")
                          .about("a hash based symmetric cryptography system")
                          .arg(Arg::with_name("encrypt")
                               .short("e")
                               .long("encrypt")
                               .help("encrypts data from stdin using KEY")
                               .takes_value(false))
                          .arg(Arg::with_name("decrypt")
                               .short("d")
                               .long("decrypt")
                               .help("decrypts data from stdin using KEY")
                               .takes_value(false))
                          .arg(Arg::with_name("keygen")
                               .short("k")
                               .long("keygen")
                               .help("Generates new key and writes key to KEY")
                               .takes_value(false))
                          .arg(Arg::with_name("KEY")
                               .help("Sets the input file to use")
                               .required(true)
                               .index(1))
                          .get_matches();

    // Calling .unwrap() is safe here because "INPUT" is required (if "INPUT" wasn't
    // required we could have used an 'if let' to conditionally get the value)
    let keyfile = matches.value_of("KEY").unwrap();
    eprintln!("Using key file: {}", keyfile);


    if matches.is_present("encrypt") {
        eprintln!("Encrypting");
        let contents = fs::read(keyfile).unwrap();
        assert_eq!(contents.len(), KEYSIZE);
        let mut buf: [u8;PTSIZE] = [0;PTSIZE];
        let mut stdin = std::io::stdin();
        let mut c: usize = stdin.read(&mut buf).unwrap();
        let mut hashlist: Vec<String> = vec![];
        while c > 0 {
            hashlist.push(encrypt_block(&buf,&contents));
            c = stdin.read(&mut buf).unwrap();
        }
        let ef = EncryptedMessage {
            hashes: hashlist,
        };
        let j = serde_json::to_string(&ef).unwrap();
        println!("{}",j);




    } else if matches.is_present("decrypt") {
        let contents = fs::read(keyfile).unwrap();
        assert_eq!(contents.len(), KEYSIZE);

        eprintln!("Decrypting");
        let mut stdin = std::io::stdin();
        let mut json = String::new();
        stdin.read_to_string(&mut json).unwrap();
        let em: EncryptedMessage = serde_json::from_str(&json).unwrap();
        decrypt(em.hashes,&contents);
    } else if matches.is_present("keygen") {
        eprintln!("Generating key");
        keygen(keyfile)
    }
}

