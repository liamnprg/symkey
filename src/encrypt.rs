use sha2::{Sha256, Digest};
use crate::cut::cut;
use crate::cut::PTSIZE;

pub fn encrypt_block(pt: &[u8;PTSIZE],key: &[u8]) -> String {
    let cut: Vec<u8> = cut(key,pt);

    let mut hasher = Sha256::new();
    hasher.update(&cut);
    let result = hasher.finalize();
    format!("{:x}",result)
}
