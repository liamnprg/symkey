use sha2::{Sha256, Digest};
use crate::cut::cut;
use crate::cut::PTSIZE;
use rayon::prelude::*;
use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;
use std::thread;
use pbr::ProgressBar;

pub fn decrypt(hashes: Vec<String>, key: &[u8]) {
    let mut plaintext = String::new();
    let (sender,reciever) = channel();
    let count = hashes.len();
    thread::spawn(move|| {
        let mut pb = ProgressBar::new(count as u64);

        for _b in reciever.iter() {
            pb.inc();
        }
        pb.finish_print("\n");
    });
    let hashsenders: Vec<(String,Sender<bool>)> = hashes.iter().map(|hash| (hash.clone(),sender.clone())).collect();
    let par_iter = hashsenders.into_par_iter().map(|hashsender| {
        let (hash,sender) = hashsender;
        let ptgot = decrypt_block(&hash,key);
        sender.send(true).unwrap();
        return ptgot;
    });
    let pts: Vec<[u8;PTSIZE]> = par_iter.collect();
    for pt in pts {
        for u in pt {
            let utt = u as u32;
            let c = char::from_u32(utt).unwrap();
            plaintext.push(c);
        }
    }
    print!("\n{}",plaintext);
}

fn decrypt_block(hash: &str, key: &[u8]) -> [u8; PTSIZE] {
    let mut pt = [0;PTSIZE];
    decrypt_block_i(hash,key,&mut pt,0);
    return pt;
}

fn decrypt_block_i(hash: &str, key: &[u8],pt: &mut [u8;PTSIZE],c: usize) -> bool {
    let mut hasher = Sha256::new();
    let cut: Vec<u8> = cut(key,pt);
    hasher.update(&cut);
    let result = hasher.finalize();
    if &format!("{:x}",result) == hash {
        return true;
    } else if c < PTSIZE {
        if pt[c] == 255 {
            pt[c] = 0;
        }
        while pt[c] < 255 {
            let found = decrypt_block_i(hash,key,pt,c+1);
            if found {
                return true;
            }
            pt[c]+=1;
        }
    }
    //c>=9
    return false;
}
//enc_pt
